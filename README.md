FacebookConnect
===============

Android Facebook Connection is about integrating facebook into your android application.

I am going to explain various steps like generating the application signature, registering facebook application, downloading facebook sdk and other steps.
Here is an official documentation of facebook integration https://developers.facebook.com/docs/android/getting-started

Generating App Signature for Facebook Settings
To create facebook android native app you need to provide your Android application signature in facebook app settings. 
You can generate your application signature (keyhash) using keytool that comes with java.
But to generate signature you need openssl installed on your pc.
If you don’t have one download openssl and set it in your system environment path.

Unzip the file file name from
C:\Users\moktar\Desktop\openssl-0.9.8k_X64.zip\bin

and copy the files-

ssley32.dll, openssl.exe and libeay32.dll then paste those file on C:\Program Files\Java\jdk1.7.0_45\bin directory


Open your command prompt (CMD) and run the following command to generate your keyhash.
While generating hashkey it should ask you password.

Give password as android.\ 

If it don’t ask for password your keystore path is incorrect.
cmd command:


keytool -exportcert -alias androiddebugkey -keystore "&lt;path-to-users-directory&gt;\.android\debug.keystore" | openssl sha1 -binary | openssl base64

example:
keytool -exportcert -alias androiddebugkey -keystore "C:\Users\Ravi\.android\debug.keystore" | openssl sha1 -binary | openssl base64

Registering your Facebook Application
After generating your app signature successfully, register your facebook application by going to create new facebook application here https://developers.facebook.com/apps and fill out all the information needed.
And select Native Android App and give your hashkey there which you generated previously using keytool.

and note down your facebook App ID

Creating Facebook Reference Project
Once you are done with registering your facebook application, you need to download facebook SDK and create a new reference project. This reference project will be used to compile your actual project.

1. Download facebook android SDK from git repositories.
(git clone https://github.com/moktar/facebook-android-sdk.git)

2. In your Eclipse goto File ⇒ Import ⇒ Existing Projects into Workspace and select the facebook project you downloaded from git repository.


Creating Your Facebook Connect Project
1. Create new Project in your Eclipse IDE. File ⇒ New ⇒ Android Project and fill out all the details.

2. Now we need to add reference of this project to existing facebook project. Right Click on Project ⇒ Properties ⇒ android ⇒ Click on Add button ⇒ select your facebook project ⇒ Click Apply.


